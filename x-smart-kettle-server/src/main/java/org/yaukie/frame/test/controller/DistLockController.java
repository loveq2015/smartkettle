package org.yaukie.frame.test.controller;

import lombok.extern.slf4j.Slf4j;

 import org.yaukie.base.annotation.EnablePage;
import org.yaukie.base.core.controller.BaseController;
import org.yaukie.base.constant.BaseResult;
import org.yaukie.base.constant.PageResult;
import org.yaukie.frame.test.service.api.DistLockService;
import org.yaukie.frame.test.model.DistLock;
import org.yaukie.frame.test.model.DistLockExample;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
 import io.swagger.annotations.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author: yuenbin
* @create: 2022/12/26 16/20/318
**/
@RestController
@RequestMapping(value = "/provider/distlock/")
@Api(value = "DistLock控制器", description = "DistLock管理")
@Slf4j
public class DistLockController  extends BaseController {

    @Autowired
    private DistLockService distLockService;

    @GetMapping(value = "/listPage")
    @ApiOperation("获取列表")
   @EnablePage
     public BaseResult getLockPageList(
                                        @RequestParam(value = "offset",required = false)String offset,
                                        @RequestParam(value = "limit",required = false)String limit,
                                         @RequestParam(value = "search",required = false)String search) {
DistLockExample distLockExample = new DistLockExample();
//    if(StringUtils.isNotBlank(search)){
//        distLockExample.createCriteria().andUserIdEqualTo(search);
//    }
     List<DistLock> distLockList = this.distLockService.selectByExample(distLockExample);
               PageResult pageResult = new PageResult(distLockList);
                Map<String, Object> result = new HashMap<>();
                result.put(RESULT_ROWS, pageResult.getRows());
                result.put(RESULT_TOTLAL, pageResult.getTotal());
                return BaseResult.success( result);
                }

                @GetMapping(value = "/get/{id}")
                    @ApiImplicitParams({
                    @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "string",paramType = "header")
                    })
               @ApiOperation("获取信息")
                public BaseResult getLock(@PathVariable String id) {
                DistLock distLock = this.distLockService.selectByPrimaryKey(Integer.parseInt(id));
                    return BaseResult.success(distLock);
                    }

                    @GetMapping(value = "/doSth")
                    @ApiOperation("执行某操作")
                    public BaseResult doSth() {
                            this.distLockService.doSth();
                            return BaseResult.success();
                    }

                    @PostMapping(value = "/add")
                    @ApiImplicitParams({
                    @ApiImplicitParam(name = "distLock"+"", value = "distLock"+"",
                    required = true,dataTypeClass =DistLock.class),
                    })
                    @ApiOperation("新增")
                    public BaseResult addLock(@RequestBody @Validated DistLock distLock, BindingResult BindingResult) {
                        if (BindingResult.hasErrors()) {
                        return this.getErrorMessage(BindingResult);
                        }
                        this.distLockService.insertSelective(distLock);
                        return BaseResult.success();
                        }

                        @PostMapping(value = "/update")
                        @ApiOperation("更新")
                        @ApiImplicitParams({
                        @ApiImplicitParam(name = "distLock"+"", value = "distLock"+"",
                            required = true,dataTypeClass =DistLock.class),
                        })
                        public BaseResult updateLock(@RequestBody @Validated DistLock distLock, BindingResult BindingResult) {
                            if (BindingResult.hasErrors()) {
                            return this.getErrorMessage(BindingResult);
                            }

                            this.distLockService.updateByPrimaryKey(distLock);
                            return BaseResult.success();
                            }

                            @GetMapping(value = "/delete/{id}")
                            @ApiOperation("删除")
                              @ApiImplicitParams({
                            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "string" ),
                            })
                            public BaseResult deleteLock(@PathVariable String id) {
                                DistLockExample distLockExample = new  DistLockExample();
                               // distLockExample.createCriteria().andIdEqualsTo(id);
                                this.distLockService.deleteByExample(distLockExample);
                                return BaseResult.success();
                                }

                                public BaseResult getErrorMessage(BindingResult BindingResult){
                                    String errorMessage = "";
                                    for (ObjectError objectError : BindingResult.getAllErrors()) {
                                    errorMessage += objectError.getDefaultMessage();
                                    }
                                    return BaseResult.fail(errorMessage);
                                    }
        }
